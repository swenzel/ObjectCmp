# ROOTObjComparer

A set of tools to inspect and compare objects serialized in ROOT files

## Examples

### Using the executable:

`ROOTCompare file.root Foo`  # prints out a textual representation of serialized entry Foo

`ROOTCompare -o file1.root -o file2.root` # field-by-field diff of 2 AliCDB objects sitting in files file1 and file2

### Using the tool inside a ROOT macro:

```C++
 TObject* obj1;
 TObject* obj2;
 
 ROOTObjComparer cmp;
 bool isDifferent = cmp.Diff(obj1, obj2);
```

# Getting the source
    git clone https://gitlab.cern.ch/swenzel/ObjectCmp

# Installation

    source <ROOT> # such that $ROOTSYS is present in the environment
    mkdir build
    cd build; cmake .. -DCMAKE_INSTALL_PREFIX=$PWD/install
    make install

# Setup

To setup the environment properly do

    source $INSTALLPATH/bin/setup
or do

* `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INSTALLPATH/lib/`
* `cd $BUILDPATH/src`

# Run
The central executable is `CompareROOT`. The first argument must be the path to the first ROOT file to be inspected. The name of the object therein is optional. If it's left out the ROOT file itself is treated
as the object of interest. In case of errors or by typing `ROOTCompare -h|--help` the follwoing help message will show up
```
Arguments:
	<filepath> [<object_name>]
	[-d | --diff : <diff_filepath> [<object name>] ]
	[ -l | --libraries : <list of additional libraries to be loaded (ROOT version < 6)> ]
	[ -i | --ignoreROOTVersion : ignore lib warning ROOT version < 6 ]
	-h | --help : Show this message and exit
```

## Print an object

```bash
ROOTCompare <ROOT file path> [<object name therein>] [-l <lib1.so> <lib2.so> <libN.so>] [-i]
```
Hence, libraries can be loaded if needed. There program will warn and abort if ROOT version < 6. This can be ignored passing the `-i` flag.

## Compare objects
```bash
ROOTCompare <filepath_1> [<object_name_therein>] -d <filepath_2> [<object_name_therein>] [-l <lib1.so> <lib2.so> <libN.so>] [-i]
```

# Test

The test can be done from `$BUILDPATH/src` after doing the [Setup](#Setup).

### make testlib known

    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INSTALLPATH/lib/

if not sourced the setup.

### create a test Foo object into `Foo.root`

    root WriteFoo.C #in the test directory

### print object

    ROOTCompare PATH_TO_FILE/Foo.root Foo #run the build/src directory

in case of ROOT5, we need to give the library (containing the Foo code in addition)

    ROOTCompare PATH_TO_FILE/Foo.root Foo -l libFoo.so #run the build/src directory
