#ifndef MYCLASS_H
#define MYCLASS_H

#include <TObject.h>
#include <Riostream.h>
#include <vector>
#include <map>
#include <TMap.h>
#include <TList.h>
#include "Foo.h"
#include <cmath>
#include <list>
#include "TRandom.h"

using std::cout;
using std::endl;

const UInt_t sizeY = 10;

class MyClass : public TObject
{

 public:
  MyClass() : TObject(),
              fN(0),
              fX(0),
              fY(),
              fV(),
              fVd(),
              fFooPtr(new Foo()),
              fstdlist(), //,
              fMap(),
              fName("hallo"),
              fName2("hallo2")
  {
    Init(5);
  }

  void Init(UInt_t size, Int_t val = 0)
  {
    delete[] fX;
    fN = size;
    fX = new Int_t[size];
    fDouble = new Double_t[size];
    memset(fX, val, fN * sizeof(Int_t));
    memset(fY, val, sizeY * sizeof(Int_t));
    fList = new TList();
    fName = TString("HUHU");
    for (UInt_t i = 0; i < fN; ++i) {
      fstdlist.push_back(Foo(i * 10, i * 101));
      fV.push_back(Foo(i, i + 1));
      fVd.push_back(gRandom->Uniform() * 1.2 * i + M_PI);
      fList->Add(new Foo(-i, -10 * i));
      fDouble[i] = M_PI * i;
      fMap.insert(std::pair<int, Foo>(i, Foo(i, i)));
    }
  }

  void SetX(UInt_t i, Int_t val)
  {
    if (i < fN)
      fX[i] = val;
    else
      cout << "Wrong index " << i << endl;
  }

  void SetY(UInt_t i, Int_t val)
  {
    if (i < sizeY)
      fY[i] = val;
    else
      cout << "Wrong index " << i << endl;
  }

  void Print(Option_t* = "")
  {
    for (UInt_t i = 0; i < fN; ++i)
      cout << i << " " << fX[i] << endl;
    for (UInt_t i = 0; i < sizeY; ++i)
      cout << i << " " << fY[i] << endl;

    cout << " the vector comes \n";
    for (UInt_t i = 0; i < fN; ++i)
      cout << i << " " << fV[i].fB << endl;

    cout << "first table entry\n";
    //TObject *obj = fMapstandard->FindObject(&fV[0]);
    //if(obj) obj->Print();
    cout << "------ " << fName << "\n";

    //for ( auto &&p : fMapstandard ){
    //  // cout << *p->first
    //}
  }

  Int_t GetN() const { return fN; }
  Int_t GetX(UInt_t i) const { return (i < fN) ? fX[i] : -1; }
  Int_t GetY(UInt_t i) const { return (i < sizeY) ? fY[i] : -1; }

  ~MyClass() {}

 private:
  ///---------- Arrays

  /// Variable size array
  UInt_t fN; ///< Size
  TList* fList;
  Int_t* fX;         //[fN] Data array
  Double_t* fDouble; //[fN]

  Int_t fY[10];
  Foo fFoo; // an embedded Foo object
  TString fName;
  std::string fName2;

  std::vector<Foo> fV;
  std::vector<double> fVd;
  std::list<Foo> fstdlist;
  Foo* fFooPtr; // pointer to Foo

  ///----- more difficult containers
  std::map<int, Foo> fMap;
  //  TMap *fMapstandard;

  ClassDef(MyClass, 1)
};

#endif
