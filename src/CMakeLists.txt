#Module
set (MODULE ROOTObjCmp)

# Additional includes - alphabetical order except ROOT
include_directories(${ROOT_INCLUDE_DIRS})
include_directories(../src)

# Sources - alphabetical order
set(SRCS ObjComparer.cxx)

# Headers from sources
string(REPLACE ".cxx" ".h" HDRS "${SRCS}")

# dictionary creation
#---include some useful Custom ROOT macros
include(CustomMacros)

get_directory_property(incdirs INCLUDE_DIRECTORIES)
generate_dictionary("${MODULE}" "${MODULE}LinkDef.h" "${HDRS}" "${incdirs}")
# Generate the ROOT map
# Dependecies
#set(LIBDEPS Core EG Geom Gpad Graf3d Graf Hist MathCore Matrix Minuit Net Physics RIO Tree)
#generate_rootmap("${MODULE}" "${LIBDEPS}" "${CMAKE_CURRENT_SOURCE_DIR}/${MODULE}LinkDef.h")


# Create an object to be reused in case of static libraries
# Otherwise the sources will be compiled twice
add_library(${MODULE}-object OBJECT ${SRCS} G__${MODULE}.cxx)
# Add a library to the project using the object
add_library(${MODULE} SHARED $<TARGET_OBJECTS:${MODULE}-object>)

add_executable(ROOTCompare ROOTCompare.cxx)
target_link_libraries(ROOTCompare ${ROOT_LIBRARIES} ${MODULE})

# Linking the library, not the object
target_link_libraries(${MODULE} ${ROOT_LIBRARIES})

# Public include folders that will be propagated to the dependecies
target_include_directories(${MODULE} PUBLIC ${incdirs})

# System dependent: Modify the way the library is build
if(${CMAKE_SYSTEM} MATCHES Darwin)
    set_target_properties(${MODULE} PROPERTIES LINK_FLAGS "-undefined dynamic_lookup")
endif(${CMAKE_SYSTEM} MATCHES Darwin)

# Installation
install(TARGETS ${MODULE}
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib)
install(TARGETS ROOTCompare DESTINATION bin)
install(FILES ${HDRS} DESTINATION include)
